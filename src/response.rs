use rocket::{
    http::{ContentType, Status},
    response::{Responder, Result},
    Request, Response,
};
use serde_json::Value;

#[derive(Debug)]
pub struct ApiResponse {
    json: Value,
    status: Status,
}

impl ApiResponse {
    pub fn new(status: Status, json: Value) -> Self {
        Self { json, status }
    }
}

#[rocket::async_trait]
impl<'r> Responder<'r, 'static> for ApiResponse {
    fn respond_to(self, req: &Request) -> Result<'static> {
        Response::build_from(self.json.respond_to(req).unwrap())
            .status(self.status)
            .header(ContentType::JSON)
            .ok()
    }
}
