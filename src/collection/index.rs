use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Arc;

#[derive(Debug, Serialize, Deserialize)]
pub struct Index {
    pub key: String,
    pub data: HashMap<Arc<str>, HashSet<Arc<str>>>,
}

impl Index {
    pub fn new(key: String) -> Self {
        log::debug!("Creating index with key {key:?}");

        Self {
            key,
            data: Default::default(),
        }
    }

    pub fn query(&self, key: &str) -> Vec<Arc<str>> {
        match self.data.get(key) {
            Some(index_set) => {
                let mut items: Vec<_> = index_set.iter().cloned().collect();
                items.sort();
                items
            }
            None => vec![],
        }
    }

    pub fn insert<K: Into<Arc<str>>>(&mut self, index_key: K, item_id: K) {
        let index_key = index_key.into();
        let item_id = item_id.into();

        match self.data.get_mut(&index_key) {
            Some(index_set) => {
                log::trace!("Inserting into index set: {index_key} -> {item_id}");
                index_set.insert(item_id);
            }
            None => {
                log::debug!("New index set {} -> {}", index_key, item_id);
                let mut index_set = HashSet::new();
                index_set.insert(item_id);
                self.data.insert(index_key, index_set);
            }
        }
    }

    #[allow(unused)]
    pub(crate) fn count_indexed_items(&self) -> usize {
        self.data.values().map(HashSet::len).sum()
    }

    #[allow(unused)]
    pub(crate) fn count_index_sets(&self) -> usize {
        self.data.len()
    }

    pub fn remove(&mut self, index_key: &str, item_id: &str) {
        if let Some(index_set) = self.data.get_mut(index_key) {
            log::trace!("Removing from index set {index_key} -> {item_id}");

            index_set.remove(item_id);

            if index_set.is_empty() {
                log::trace!("Index set for {index_key:?} is empty, removing from index");
                self.data.remove(index_key);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_indexing() {
        let mut index = Index::new("name".to_owned());
        assert!(index.query("name").is_empty());
        assert_eq!(index.count_indexed_items(), 0);
        assert_eq!(index.count_index_sets(), 0);

        index.insert("name", "a");
        assert_eq!(index.query("name").len(), 1);
        assert_eq!(index.count_indexed_items(), 1);
        assert_eq!(index.count_index_sets(), 1);

        index.insert("name", "b");
        assert_eq!(index.query("name").len(), 2);
        assert_eq!(index.count_indexed_items(), 2);
        assert_eq!(index.count_index_sets(), 1);

        index.remove("name", "b");
        assert_eq!(index.query("name").len(), 1);
        assert_eq!(index.count_indexed_items(), 1);
        assert_eq!(index.count_index_sets(), 1);

        index.insert("other", "c");
        index.insert("other", "d");
        assert_eq!(index.query("other").len(), 2);
        assert_eq!(index.count_indexed_items(), 3);
        assert_eq!(index.count_index_sets(), 2);

        index.remove("name", "a");
        assert!(index.query("name").is_empty());
        assert_eq!(index.query("other").len(), 2);
        assert_eq!(index.count_indexed_items(), 2);
        assert_eq!(index.count_index_sets(), 1);

        index.remove("other", "c");
        index.remove("other", "d");
        assert!(index.query("name").is_empty());
        assert!(index.query("other").is_empty());
        assert_eq!(index.count_indexed_items(), 0);
        assert_eq!(index.count_index_sets(), 0);
    }
}
