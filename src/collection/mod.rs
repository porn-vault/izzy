pub mod index;
pub mod routes;

use crate::util::{stringify_json, update_shallow};
use index::Index;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::{BTreeMap, HashMap};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};
use std::sync::Arc;

pub struct Collection {
    #[allow(dead_code)]
    name: String,

    writer: BufWriter<File>,
    file: PathBuf,

    pub data: BTreeMap<Arc<str>, Vec<u8>>,
    pub indexes: HashMap<String, Index>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct IndexCreateOptions {
    name: String,
    key: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct QueryBulkOptions {
    keys: Vec<String>,
}

impl Drop for Collection {
    fn drop(&mut self) {
        if let Err(e) = self.flush() {
            log::error!("Flush failed: {e:?}");
        }
    }
}

impl Collection {
    fn new<P: AsRef<Path>>(name: &str, path: P) -> std::io::Result<Self> {
        let path = path.as_ref();

        log::debug!("Creating collection at {path:?}");

        let file_handle = OpenOptions::new().create(true).append(true).open(path)?;

        Ok(Self {
            writer: BufWriter::new(file_handle),
            name: name.to_owned(),
            data: Default::default(),
            file: path.into(),
            indexes: HashMap::new(),
        })
    }

    pub fn create_or_recover<P: AsRef<Path>>(
        path: P,
        name: &str,
        indexes: &[IndexCreateOptions],
    ) -> std::io::Result<Self> {
        let path = path.as_ref();

        let mut collection = Self::new(name, path)?;

        for index in indexes {
            // Create index
            let created_index = Index::new(index.key.clone());
            collection.indexes.insert(index.name.clone(), created_index);
        }

        if path.exists() {
            log::debug!("Recovering file at {path:?}");

            let file = File::open(path).unwrap();
            let reader = BufReader::new(file);

            for (idx, line) in reader.lines().enumerate() {
                let line = line.unwrap();

                if !line.is_empty() {
                    let json_content = serde_json::from_str(&line);

                    let json_content: Value = match json_content {
                        Ok(v) => v,
                        Err(e) => {
                            log::error!("Failed to recover from {path:?}: invalid json >>> line {idx} {line}");

                            return Err(std::io::Error::new(
                                std::io::ErrorKind::Other,
                                format!("Could not parse JSON: {e:?}"),
                            ));
                        }
                    };
                    if !json_content.is_object() {
                        log::error!(
                            "Failed to recover from {path:?}: not an object >>> line {idx} {line}"
                        );

                        return Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            format!("Could not parse JSON: not an object"),
                        ));
                    }

                    if json_content["$$indexCreated"].is_object() {
                        // do nothing
                    } else {
                        let id = json_content["_id"].as_str();
                        let id = match id {
                            Some(v) => v,
                            None => {
                                return Err(std::io::Error::new(
                                    std::io::ErrorKind::Other,
                                    format!("Could not parse JSON: missing primary key _id"),
                                ))
                            }
                        };

                        if json_content["$$deleted"].is_boolean() {
                            collection.internal_remove(id);
                        } else {
                            collection.internal_upsert(id, &json_content);
                        }
                    }
                }
            }
        }

        Ok(collection)
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn query_bulk(
        &self,
        index: &str,
        input: &QueryBulkOptions,
    ) -> Option<HashMap<String, Vec<Value>>> {
        let index = self.indexes.get(index)?;

        let mut result = HashMap::new();

        for key in &input.keys {
            if let Some(result_set) = index.data.get(&**key) {
                let mut results: Vec<_> = result_set
                    .iter()
                    .map(|x| self.data.get(&**x))
                    .flatten()
                    .map(|x| miniz_oxide::inflate::decompress_to_vec(x).unwrap())
                    .collect();

                results.sort();

                let results = results
                    .into_iter()
                    .map(|x| rmp_serde::from_slice(&x).unwrap())
                    .collect();

                result.insert(key.clone(), results);
            };
        }

        Some(result)
    }

    pub fn remove_indexed(&mut self, index_name: &str, key: &str) -> std::io::Result<usize> {
        if let Some(index) = self.indexes.get_mut(index_name) {
            if let Some(deleted_ids) = index.data.remove(key) {
                for id in &deleted_ids {
                    self.add_remove_entry(&id)?;
                }
                self.flush()?;

                return Ok(deleted_ids.len());
            }
        } else {
            log::warn!("Tried to delete indexed from index {index_name:?} (does not exist)");
        }

        Ok(0)
    }

    pub fn partial_update<K: Into<Arc<str>>>(
        &mut self,
        id: K,
        delta: &Value,
    ) -> std::io::Result<Value> {
        let id: Arc<str> = id.into();

        if let Some(prev) = self.get(&*id) {
            let new_value = update_shallow(&prev, delta);
            self.upsert(&id, &new_value)?;
            Ok(new_value)
        } else {
            Ok(Value::Null)
        }
    }

    fn internal_upsert<K: Into<Arc<str>>>(&mut self, id: K, value: &Value) {
        let id: Arc<str> = id.into();

        if let Some(prev) = self.get(&id) {
            self.remove_from_indexes(&id, &prev);
        }

        let serialized = rmp_serde::to_vec(value).unwrap();
        let compressed = miniz_oxide::deflate::compress_to_vec(&serialized, 10);
        self.data.insert(id.clone(), compressed);

        for (index_name, index) in self.indexes.iter_mut() {
            let key = value[index.key.clone()].as_str().unwrap_or("$$null");
            log::trace!("Indexing {index_name:?}/{key:?}/{id:?}");
            index.insert(key, &id);
        }
    }

    pub fn upsert(&mut self, id: &str, value: &Value) -> std::io::Result<()> {
        let line = stringify_json(&value);

        writeln!(self.writer, "{line}")?;
        self.flush()?;

        self.internal_upsert(id, value);

        Ok(())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.writer.flush()?;
        self.writer.get_mut().sync_all()
    }

    fn remove_from_indexes(&mut self, id: &str, item: &Value) {
        for (index_name, index) in self.indexes.iter_mut() {
            let key = item[index.key.clone()].as_str().unwrap_or("$$null");
            log::trace!("Unindexing {:?}/{:?}/{:?}", index_name, key, id);
            index.remove(key, id);
        }
    }

    fn add_remove_entry(&mut self, id: &str) -> std::io::Result<()> {
        let line = format!("{{\"$$deleted\":true,\"_id\":\"{}\"}}", id);
        writeln!(self.writer, "{line}")
    }

    fn internal_remove(&mut self, id: &str) -> Value {
        if let Some(bytes) = self.data.remove(id) {
            let value: Value =
                rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(&bytes).unwrap())
                    .unwrap();
            self.remove_from_indexes(id, &value);
            value
        } else {
            Value::Null
        }
    }

    pub fn remove(&mut self, id: &str) -> std::io::Result<Value> {
        self.add_remove_entry(id)?;
        self.flush()?;
        Ok(self.internal_remove(id))
    }

    pub fn compact(&mut self) -> std::io::Result<()> {
        self.flush()?;

        let filename = format!("{}~", self.file.display());

        log::debug!("Creating tmp file for compaction at {filename:?}");
        let mut file = BufWriter::new(File::create(&filename)?);

        log::debug!("Writing tmp file");
        for value in self.data.values() {
            let value: Value =
                rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(&value).unwrap())
                    .unwrap();
            writeln!(file, "{value}")?;
        }

        log::info!(
            "Finalising compaction {} -> {}",
            filename,
            self.file.display()
        );

        std::fs::rename(filename, &self.file)?;

        let file_handle = OpenOptions::new().append(true).open(&self.file)?;
        self.writer = BufWriter::new(file_handle);

        Ok(())
    }

    pub fn get(&self, id: &str) -> Option<Value> {
        self.data.get(id).map(|x| {
            rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(x).unwrap()).unwrap()
        })
    }

    pub fn query(&self, index: &str, key: &str) -> Option<Vec<Value>> {
        let index = self.indexes.get(index)?;

        let results = index.query(&key);

        let results = results
            .into_iter()
            .map(|x| self.get(&x))
            .flatten()
            .collect::<Vec<_>>();

        Some(results)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::json;

    #[test]
    fn compact_and_recover() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        {
            let mut db = Collection::create_or_recover(&file, "name", &[])?;
            assert_eq!(0, db.len());

            db.compact()?;

            db.upsert(
                "1",
                &json!({
                    "_id": "1",
                    "name": "1",
                }),
            )?;

            assert!(db.get("1").is_some());
            assert_eq!(1, db.len());
        }

        {
            let db = Collection::create_or_recover(file, "name", &[])?;
            assert!(db.get("1").is_some());
            assert_eq!(1, db.len());
        }

        Ok(())
    }

    #[test]
    fn compact_and_recover_2() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        {
            let mut db = Collection::create_or_recover(&file, "name", &[])?;
            assert_eq!(0, db.len());

            db.compact()?;

            db.upsert(
                "1",
                &json!({
                    "_id": "1",
                    "name": "1",
                }),
            )?;

            assert!(db.get("1").is_some());
            assert_eq!(1, db.len());

            db.upsert(
                "2",
                &json!({
                    "_id": "2",
                    "name": "2",
                }),
            )?;

            assert!(db.get("1").is_some());
            assert!(db.get("2").is_some());
            assert_eq!(2, db.len());
        }

        {
            let mut db = Collection::create_or_recover(&file, "name", &[])?;
            assert!(db.get("1").is_some());
            assert!(db.get("2").is_some());
            assert_eq!(2, db.len());

            db.compact()?;

            assert!(db.get("1").is_some());
            assert!(db.get("2").is_some());
            assert_eq!(2, db.len());
        }

        {
            let db = Collection::create_or_recover(&file, "name", &[])?;
            assert!(db.get("1").is_some());
            assert!(db.get("2").is_some());
            assert_eq!(2, db.len());
        }

        Ok(())
    }

    #[test]
    fn recover_after_write() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        {
            let mut db = Collection::create_or_recover(&file, "name", &[])?;
            assert_eq!(0, db.len());

            db.upsert(
                "1",
                &json!({
                    "_id": "1",
                    "name": "1",
                    "movie": "1"
                }),
            )?;

            assert!(db.get("1").is_some());
            assert_eq!(1, db.len());
        }

        {
            let db = Collection::create_or_recover(file, "name", &[])?;
            assert!(db.get("1").is_some());
            assert_eq!(1, db.len());
        }

        Ok(())
    }

    #[test]
    fn reindex() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(
            file,
            "name",
            &[IndexCreateOptions {
                name: "movie".to_owned(),
                key: "movie".to_owned(),
            }],
        )?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "1"
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "1".to_owned(),
                    vec![json!({
                        "_id": "1",
                        "name": "1",
                        "movie": "1"
                    })],
                );

                map
            })
        );

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "2"
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned()]
                }
            ),
            Some(HashMap::new())
        );

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["2".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "2".to_owned(),
                    vec![json!({
                        "_id": "1",
                        "name": "1",
                        "movie": "2"
                    })],
                );

                map
            })
        );

        Ok(())
    }

    #[test]
    fn indexed_delete() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(
            file,
            "name",
            &[IndexCreateOptions {
                name: "movie".to_owned(),
                key: "movie".to_owned(),
            }],
        )?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "1"
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "1".to_owned(),
                    vec![json!({
                        "_id": "1",
                        "name": "1",
                        "movie": "1"
                    })],
                );

                map
            })
        );

        db.remove("1")?;

        assert!(db.indexes.get("movie").unwrap().data.is_empty());

        Ok(())
    }

    #[test]
    fn reindex_partial_update() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(
            file,
            "name",
            &[IndexCreateOptions {
                name: "movie".to_owned(),
                key: "movie".to_owned(),
            }],
        )?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "1"
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "1".to_owned(),
                    vec![json!({
                        "_id": "1",
                        "name": "1",
                        "movie": "1"
                    })],
                );

                map
            })
        );

        db.partial_update(
            "1",
            &json!({
                "movie": "2"
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned()]
                }
            ),
            Some(HashMap::new())
        );

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["2".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "2".to_owned(),
                    vec![json!({
                        "_id": "1",
                        "name": "1",
                        "movie": "2"
                    })],
                );

                map
            })
        );

        Ok(())
    }

    #[test]
    fn query_bulk() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(
            file,
            "name",
            &[IndexCreateOptions {
                name: "movie".to_owned(),
                key: "movie".to_owned(),
            }],
        )?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "1"
            }),
        )?;
        db.upsert(
            "2",
            &json!({
                "_id": "2",
                "name": "2",
                "movie": "1"
            }),
        )?;
        db.upsert(
            "3",
            &json!({
                "_id": "3",
                "name": "3",
                "movie": "2"
            }),
        )?;
        db.upsert(
            "4",
            &json!({
                "_id": "4",
                "name": "4",
            }),
        )?;

        assert_eq!(
            db.query_bulk(
                "movie",
                &QueryBulkOptions {
                    keys: vec!["1".to_owned(), "2".to_owned()]
                }
            ),
            Some({
                let mut map = HashMap::new();

                map.insert(
                    "1".to_owned(),
                    vec![
                        json!({
                            "_id": "1",
                            "name": "1",
                            "movie": "1"
                        }),
                        json!({
                            "_id": "2",
                            "name": "2",
                            "movie": "1"
                        }),
                    ],
                );
                map.insert(
                    "2".to_owned(),
                    vec![json!({
                        "_id": "3",
                        "name": "3",
                        "movie": "2"
                    })],
                );

                map
            })
        );

        Ok(())
    }

    #[test]
    fn query_indexed() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(
            file,
            "name",
            &[IndexCreateOptions {
                name: "movie".to_owned(),
                key: "movie".to_owned(),
            }],
        )?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
                "movie": "1"
            }),
        )?;
        db.upsert(
            "2",
            &json!({
                "_id": "2",
                "name": "2",
                "movie": "1"
            }),
        )?;
        db.upsert(
            "3",
            &json!({
                "_id": "3",
                "name": "3",
                "movie": "2"
            }),
        )?;

        assert_eq!(
            db.query("movie", "1"),
            Some(vec![
                json!({
                    "_id": "1",
                    "name": "1",
                    "movie": "1"
                }),
                json!({
                    "_id": "2",
                    "name": "2",
                    "movie": "1"
                }),
            ])
        );

        assert_eq!(
            db.query("movie", "2"),
            Some(vec![json!({
                "_id": "3",
                "name": "3",
                "movie": "2"
            })])
        );

        Ok(())
    }

    #[test]
    fn write_and_read_by_id() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(file, "name", &[])?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
            }),
        )?;

        assert_eq!(
            db.get("1"),
            Some(json!({
                "_id": "1",
                "name": "1",
            }))
        );

        db.remove("1")?;

        assert_eq!(db.get("1"), None);

        Ok(())
    }

    #[test]
    fn upsert() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(file, "name", &[])?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
            }),
        )?;
        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "v2",
            }),
        )?;

        assert_eq!(
            db.get("1"),
            Some(json!({
                "_id": "1",
                "name": "v2",
            }))
        );

        Ok(())
    }

    #[test]
    fn partial_update() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        let mut db = Collection::create_or_recover(file, "name", &[])?;

        db.upsert(
            "1",
            &json!({
                "_id": "1",
                "name": "1",
            }),
        )?;
        db.partial_update(
            "1",
            &json!({
                "name": "v2",
                "something_new": true
            }),
        )?;

        assert_eq!(
            db.get("1"),
            Some(json!({
                "_id": "1",
                "name": "v2",
                "something_new": true,
            }))
        );

        Ok(())
    }

    #[test]
    fn recovery_fail() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        std::fs::write(&file, "46\n")?;

        let recovery_result = Collection::create_or_recover(file, "name", &[]);
        assert!(recovery_result.is_err());

        Ok(())
    }

    #[test]
    fn recovery_fail_2() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        std::fs::write(&file, "{\"xyz\": \"asd\"}\n")?;

        let recovery_result = Collection::create_or_recover(file, "name", &[]);
        assert!(recovery_result.is_err());

        Ok(())
    }

    #[test]
    fn recovery_success() -> std::io::Result<()> {
        let folder = tempfile::tempdir()?;
        let file = folder.path().join("test.db");

        std::fs::write(&file, "{\"_id\": \"asd\"}\n")?;

        let collection = Collection::create_or_recover(file, "name", &[])?;
        assert_eq!(1, collection.len());

        Ok(())
    }
}
