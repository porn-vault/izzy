use crate::collection::index::Index;
use crate::collection::{Collection, IndexCreateOptions, QueryBulkOptions};
use crate::response::ApiResponse;
use rocket::http::Status;
use rocket::serde::json::{json, Json};
use rocket::State;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::{Arc, RwLock};
use std::vec::Vec;

#[derive(Default)]
pub struct MyState {
    collections: RwLock<HashMap<String, Collection>>,
}

#[post("/compact/<name>", rank = 0)]
fn compact_collection(state: &State<Arc<MyState>>, name: &str) -> Status {
    log::info!("Compacting collection {name:?}");

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return Status::NotFound;
    };

    match collection.compact() {
        Ok(_) => Status::Ok,
        Err(e) => {
            log::error!("compaction error: {e:?}");
            Status::InternalServerError
        }
    }
}

#[delete("/<name>/<id>")]
fn delete_item(state: &State<Arc<MyState>>, name: &str, id: &str) -> ApiResponse {
    log::debug!("Deleting {name}/{id}");

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return response_404("Collection not found");
    };

    if !collection.data.contains_key(id) {
        response_404("Item not found")
    } else {
        let item = collection.remove(id).unwrap();
        ApiResponse::new(Status::Ok, json!(item))
    }
}

#[delete("/<name>/<index>/<key>")]
fn delete_indexed(state: &State<Arc<MyState>>, name: &str, index: &str, key: &str) -> ApiResponse {
    log::debug!(target: "collection", "Deleting indexed {name}/{index}/{key}");

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return response_404("Collection not found");
    };

    let count = collection.remove_indexed(index, key).unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
            "status": 200,
            "message": "OK",
            "count": count,
        }),
    )
}

#[get("/<name>/<index>/<key>")]
fn retrieve_indexed(
    state: &State<Arc<MyState>>,
    name: &str,
    index: &str,
    key: Option<&str>,
) -> ApiResponse {
    let key = match key {
        Some(value) => value.to_owned(),
        None => String::from("$$null"),
    };

    log::debug!(target: "collection", "Retrieving indexed {:?}/{:?}/{:?}", name, index, key);

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    let items = collection.query(index, &key);

    let status = if items.is_some() {
        Status::Ok
    } else {
        Status::NotFound
    };

    ApiResponse::new(status, json!({ "items": items }))
}

#[get("/<name>/head", rank = 0)]
fn retrieve_head(state: &State<Arc<MyState>>, name: &str) -> ApiResponse {
    log::debug!("Retrieve head of {:?}", name);

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    let item = collection
        .data
        .values()
        .next()
        .map(|x| {
            rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(&x).unwrap()).unwrap()
        })
        .unwrap_or(Value::Null);

    ApiResponse::new(Status::Ok, item)
}

#[get("/<name>/tail?<skip>&<take>", rank = 0)]
fn retrieve_tail(state: &State<Arc<MyState>>, name: &str, skip: usize, take: usize) -> ApiResponse {
    log::debug!("Retrieve tail of {:?} [s: {skip}, t: {take}]", name);

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    let items: Vec<Value> = collection
        .data
        .values()
        .rev()
        .skip(skip)
        .take(take)
        .map(|x| {
            rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(&x).unwrap()).unwrap()
        })
        .collect();

    ApiResponse::new(Status::Ok, Value::Array(items))
}

#[get("/<name>/<id>", rank = 1)]
fn retrieve_item(state: &State<Arc<MyState>>, name: &str, id: &str) -> ApiResponse {
    log::debug!("Retrieve {name}/{id}");

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    if let Some(item) = collection.get(id) {
        ApiResponse::new(Status::Ok, item)
    } else {
        response_404("Item not found")
    }
}

#[post("/<name>/index/<index>/query-bulk", data = "<input>")]
fn query_bulk(
    state: &State<Arc<MyState>>,
    name: &str,
    index: &str,
    input: Json<QueryBulkOptions>,
) -> ApiResponse {
    log::debug!("Bulk-query indexed {name:?}/{index:?}");

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    let result = collection.query_bulk(index, &input.into_inner());

    log::debug!(
        "Returning {:?} items from query-bulk on {name}/{index}",
        result.as_ref().map(HashMap::len).unwrap_or_default(),
    );

    let status = if result.is_some() {
        Status::Ok
    } else {
        Status::NotFound
    };

    ApiResponse::new(status, json!(result))
}

#[derive(Clone, Serialize, Deserialize)]
struct BulkOptions {
    items: Vec<String>,
}

#[post("/<name>/bulk", data = "<input>")]
fn retrieve_bulk(state: &State<Arc<MyState>>, name: &str, input: Json<BulkOptions>) -> ApiResponse {
    log::debug!("Retrieving bulk from {name:?}");

    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    let results: Vec<_> = input
        .into_inner()
        .items
        .into_iter()
        .map(|x| collection.get(&*x))
        .collect();

    ApiResponse::new(Status::Ok, json!({ "items": results }))
}

#[post("/<name>/<id>", data = "<input>", rank = 1)]
fn insert_item(state: &State<Arc<MyState>>, name: &str, id: &str, input: Json<Value>) -> Status {
    log::info!("Inserting {:?}/{:?}", name, id);

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return Status::NotFound;
    };

    collection.upsert(id, &input.into_inner()).unwrap();
    Status::Ok
}

#[patch("/<name>/<id>", data = "<input>", rank = 1)]
fn partial_update_item(
    state: &State<Arc<MyState>>,
    name: &str,
    id: &str,
    input: Json<Value>,
) -> ApiResponse {
    log::info!("Inserting {name}/{id}");

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return response_404("Collection not found");
    };

    let new_value = collection.partial_update(id, &input.into_inner()).unwrap();
    ApiResponse::new(Status::Ok, new_value)
}

#[derive(Clone, Serialize, Deserialize)]
struct CollectionData {
    file: PathBuf,
    indexes: Vec<IndexCreateOptions>,
}

#[derive(Clone, Serialize, Deserialize)]
struct IndexData {
    key: String,
}

#[post("/<name>/index/<index>", data = "<data>")]
fn create_index(
    state: &State<Arc<MyState>>,
    name: &str,
    index: &str,
    data: Json<IndexData>,
) -> Status {
    log::info!("Creating index {name}/{index}");

    let mut collection_map = state.collections.write().unwrap();

    let Some(collection) = collection_map.get_mut(name) else {
        return Status::NotFound;
    };

    if let std::collections::hash_map::Entry::Vacant(e) = collection.indexes.entry(index.to_owned())
    {
        // Create index
        let created_index = Index::new(data.key.clone());
        e.insert(created_index);
        Status::Ok
    } else {
        Status::Conflict
    }
}

#[post("/<name>", data = "<data>", rank = 7)]
fn create(state: &State<Arc<MyState>>, name: &str, data: Json<CollectionData>) -> Status {
    log::info!("Creating collection {name:?}");

    let mut collection_map = state.collections.write().unwrap();

    if collection_map.contains_key(name) {
        return Status::Conflict;
    }

    let collection = Collection::create_or_recover(&data.file, &name, &data.indexes).unwrap();

    collection_map.insert(name.to_owned(), collection);

    log::info!("Collection {name:?} created");

    Status::Ok
}

#[delete("/<name>")]
fn delete_collection(state: &State<Arc<MyState>>, name: &str) -> ApiResponse {
    log::info!("Deleting collection {name:?}");

    let mut collection_map = state.collections.write().unwrap();

    match collection_map.get(name) {
        Some(_) => {
            collection_map.remove(name);
            ApiResponse::new(Status::Ok, json!("null"))
        }
        None => response_404("Collection not found"),
    }
}

#[get("/<name>")]
fn get_collection(state: &State<Arc<MyState>>, name: &str) -> ApiResponse {
    let collection_map = state.collections.read().unwrap();

    match collection_map.get(name) {
        Some(collection) => {
            let results: Vec<_> = collection.data.values().collect();
            let parsed_results: Vec<Value> = results
                .into_iter()
                .map(|x| {
                    rmp_serde::from_slice(&miniz_oxide::inflate::decompress_to_vec(&x).unwrap())
                        .unwrap()
                })
                .collect();
            ApiResponse::new(Status::Ok, json!({ "items": parsed_results }))
        }
        None => response_404("Collection not found"),
    }
}

fn response_404(msg: &str) -> ApiResponse {
    ApiResponse::new(
        Status::NotFound,
        json!({
            "status": 404,
            "message":msg,
            "error": true
        }),
    )
}

#[get("/<name>/count")]
fn get_count(state: &State<Arc<MyState>>, name: &str) -> ApiResponse {
    let collection_map = state.collections.read().unwrap();

    let Some(collection) = collection_map.get(name) else {
        return response_404("Collection not found");
    };

    ApiResponse::new(
        Status::Ok,
        json!({
          "count": collection.len()
        }),
    )
}

#[delete("/")]
fn reset(state: &State<Arc<MyState>>) -> Status {
    let mut collection_map = state.collections.write().unwrap();
    *collection_map = Default::default();
    Status::Ok
}

pub fn routes() -> std::vec::Vec<rocket::Route> {
    routes![
        retrieve_head,
        retrieve_tail,
        retrieve_bulk,
        query_bulk,
        get_count,
        compact_collection,
        get_collection,
        create_index,
        create,
        reset,
        delete_collection,
        insert_item,
        retrieve_item,
        retrieve_indexed,
        delete_item,
        delete_indexed,
        partial_update_item // TODO: delete_bulk
    ]
}
