use serde_json::Value;

pub fn stringify_json(datastr: &Value) -> String {
    serde_json::to_string(&datastr).unwrap()
}

pub fn update_shallow(target: &Value, diff: &Value) -> Value {
    let mut copy = target.clone();
    let copy = copy.as_object_mut().unwrap();

    let obj = diff.as_object().unwrap();

    for (key, value) in obj {
        copy.insert(key.clone(), value.clone());
    }

    Value::Object(copy.clone())
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::json;

    pub fn parse_json(str: &str) -> Value {
        serde_json::from_str(str).unwrap()
    }

    #[test]
    fn test_update_shallow() {
        let value_before = json!({
            "name": "Test",
            "age": 25
        });

        let diff = json!({
            "age": 23,
            "new_array": [1, 2, 3]
        });

        let object_after = update_shallow(&value_before, &diff);

        assert_eq!(
            json!({
                "name": "Test",
                "age": 23,
                "new_array": [1, 2, 3]
            }),
            object_after
        );
    }

    #[test]
    fn test_stringify_and_back() {
        let value = json!({
            "name": "Test",
            "age": 25
        });

        let stringified = stringify_json(&value);
        let parsed = parse_json(&stringified);

        assert_eq!(value, parsed);
    }
}
