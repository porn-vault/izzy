use super::{SchemaDef, SearchIndex};
use crate::response::ApiResponse;
use rocket::{http::Status, serde::json::Json, State};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::{
    collections::HashMap,
    path::PathBuf,
    sync::{Arc, RwLock},
};
use tantivy::Order;

fn response_404(msg: &str) -> ApiResponse {
    ApiResponse::new(
        Status::NotFound,
        json!({
            "status": 404,
            "message":msg,
            "error": true
        }),
    )
}

#[derive(Default)]
pub struct MyState {
    indexes: RwLock<HashMap<String, SearchIndex>>,
}

#[derive(Deserialize, Serialize)]
pub struct IndexCreateOptions {
    path: PathBuf,
    schema: SchemaDef,
}

#[derive(Deserialize, Serialize)]
pub struct BulkRemoveOptions {
    items: Vec<String>,
}

#[put("/<name>", data = "<data>", rank = 7)]
fn create(state: &State<Arc<MyState>>, name: &str, data: Json<IndexCreateOptions>) -> Status {
    log::info!("Creating search index {name:?}");

    let mut index_map = state.indexes.write().unwrap();

    if index_map.contains_key(name) {
        return Status::Conflict;
    }

    let data = data.into_inner();

    let index = SearchIndex::new(name, &data.path, data.schema).unwrap();
    index_map.insert(name.to_owned(), index);

    log::info!("Search index {name:?} created");

    Status::Ok
}

#[post("/<name>/items", data = "<input>")]
fn index_items(state: &State<Arc<MyState>>, name: &str, input: Json<Vec<Value>>) -> ApiResponse {
    let mut index_map = state.indexes.write().unwrap();

    let Some(index) = index_map.get_mut(name) else {
        return response_404("Search index not found");
    };

    let input = input.into_inner();

    for item in &input {
        let id = item["_id"].as_str().unwrap();
        index.remove_item(id, false).unwrap();
    }

    index.index_items(&input, true).unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
          "error": false,
        }),
    )
}

#[delete("/<name>/items/<id>")]
fn remove_item(state: &State<Arc<MyState>>, name: &str, id: &str) -> ApiResponse {
    let mut index_map = state.indexes.write().unwrap();

    let Some(index) = index_map.get_mut(name) else {
        return response_404("Search index not found");
    };

    index.remove_item(id, true).unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
          "error": false,
        }),
    )
}

#[delete("/<name>/items", data = "<data>")]
fn bulk_remove(
    state: &State<Arc<MyState>>,
    name: &str,
    data: Json<BulkRemoveOptions>,
) -> ApiResponse {
    let mut index_map = state.indexes.write().unwrap();

    let Some(index) = index_map.get_mut(name) else {
        return response_404("Search index not found");
    };

    index.bulk_remove(&data.items, true).unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
          "error": false,
        }),
    )
}

#[get("/<name>/count?<q>")]
fn get_count(state: &State<Arc<MyState>>, name: &str, q: Option<&str>) -> ApiResponse {
    let index_map = state.indexes.read().unwrap();

    let Some(index) = index_map.get(name) else {
        return response_404("Search index not found");
    };

    ApiResponse::new(
        Status::Ok,
        json!({
          "count": index.count(q).unwrap(),
        }),
    )
}

#[get("/<name>?<q>&<skip>&<take>&<order_dir>&<order_by>")]
fn query(
    state: &State<Arc<MyState>>,
    name: &str,
    q: &str,
    skip: Option<usize>,
    take: Option<usize>,
    order_by: Option<&str>,
    order_dir: Option<&str>,
) -> ApiResponse {
    let index_map = state.indexes.read().unwrap();

    let Some(index) = index_map.get(name) else {
        return response_404("Search index not found");
    };

    let order = match order_dir {
        Some("asc") => Some(Order::Asc),
        Some("desc") => Some(Order::Desc),
        Some(_) => None,
        None => None,
    };

    let result = match (order_by, order) {
        (Some(order_by), Some(dir)) => index.search_ordered(
            q,
            skip.unwrap_or_default(),
            take.unwrap_or(10),
            (order_by, dir),
        ),
        (_, _) => index.search(q, skip.unwrap_or_default(), take.unwrap_or(10)),
    }
    .unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
          "result": result,
        }),
    )
}

#[post("/<name>/aggregate", data = "<input>")]
fn aggregate(state: &State<Arc<MyState>>, name: &str, input: Json<Value>) -> ApiResponse {
    let index_map = state.indexes.read().unwrap();

    let Some(index) = index_map.get(name) else {
        return response_404("Search index not found");
    };

    let input = input.into_inner();

    let result = index
        .aggregate(serde_json::from_value(input).unwrap())
        .unwrap();

    ApiResponse::new(
        Status::Ok,
        json!({
          "result": result,
        }),
    )
}

#[delete("/<name>")]
fn delete_index(state: &State<Arc<MyState>>, name: &str) -> ApiResponse {
    let mut index_map = state.indexes.write().unwrap();

    if let Some(index) = index_map.remove(name) {
        let path = index.path;

        std::fs::remove_dir_all(path).unwrap();

        ApiResponse::new(
            Status::Ok,
            json!({
              "error": false,
            }),
        )
    } else {
        return response_404("Search index not found");
    }
}

pub fn routes() -> std::vec::Vec<rocket::Route> {
    routes![
        create,
        get_count,
        index_items,
        query,
        aggregate,
        remove_item,
        bulk_remove,
        delete_index,
    ]
}
