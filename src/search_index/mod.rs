pub mod routes;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};
use tantivy::{
    aggregation::{agg_req::Aggregations, agg_result::AggregationResults},
    collector::TopDocs,
    query::{AllQuery, Query, QueryParser},
    ReloadPolicy,
};

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
enum FieldType {
    Text,
    Keyword,
    Integer,
    Json,
    Boolean,
}

type SchemaDef = HashMap<String, FieldType>;

#[derive(Debug, Deserialize, Serialize)]
pub struct SearchResult {
    hits_count: usize,
    ids: Vec<String>,
}

pub struct SearchIndex {
    #[allow(dead_code)]
    name: String,

    path: PathBuf,

    schema_def: SchemaDef,

    data: tantivy::Index,

    index_writer: tantivy::IndexWriter,
    index_reader: tantivy::IndexReader,
}

impl SearchIndex {
    pub fn aggregate(&self, agg: Aggregations) -> std::io::Result<AggregationResults> {
        use tantivy::aggregation::{AggregationCollector, AggregationLimits};

        let collector = AggregationCollector::from_aggs(agg, AggregationLimits::default());
        let searcher = self.index_reader.searcher();

        Ok(searcher.search(&AllQuery, &collector).unwrap())
    }

    pub fn count(&self, query: Option<&str>) -> std::io::Result<usize> {
        let schema = self.data.schema();
        let id_field = schema.get_field("_id").unwrap();
        let searcher = self.index_reader.searcher();

        let query = match query {
            Some(qs) => {
                let query_parser = QueryParser::for_index(&self.data, vec![id_field]);
                query_parser.parse_query(qs).expect("should parse query")
            }
            None => Box::new(AllQuery),
        };

        let count = query.count(&searcher).expect("should count");
        Ok(count)
    }

    pub fn search_ordered(
        &self,
        query: &str,
        skip: usize,
        take: usize,
        order_by: (&str, tantivy::Order),
    ) -> std::io::Result<SearchResult> {
        let schema = self.data.schema();
        let id_field = schema.get_field("_id").unwrap();

        let searcher = self.index_reader.searcher();

        let query = if query.is_empty() {
            Box::new(AllQuery)
        } else {
            let query_parser = QueryParser::for_index(&self.data, vec![id_field]);
            query_parser.parse_query(query).expect("should parse query")
        };

        let count = query.count(&searcher).expect("should get count");

        log::debug!("Query: {query:?}");

        let collector = TopDocs::with_limit(take)
            .and_offset(skip)
            .order_by_fast_field(order_by.0, order_by.1);

        let top_docs: Vec<(i64, _)> = searcher.search(&query, &collector).expect("should rank");

        let ids = top_docs
            .into_iter()
            .map(|(_, doc_address)| {
                use tantivy::schema::Value;

                let retrieved_doc: tantivy::TantivyDocument =
                    searcher.doc(doc_address).expect("should get doc");

                retrieved_doc
                    .get_first(id_field)
                    .expect("should retrieve id")
                    .as_str()
                    .expect("should get id")
                    .to_owned()
            })
            .collect();

        Ok(SearchResult {
            hits_count: count,
            ids,
        })
    }

    pub fn search(&self, query: &str, skip: usize, take: usize) -> std::io::Result<SearchResult> {
        let schema = self.data.schema();
        let id_field = schema.get_field("_id").unwrap();

        let searcher = self.index_reader.searcher();

        let query = if query.is_empty() {
            Box::new(AllQuery)
        } else {
            let query_parser = QueryParser::for_index(&self.data, vec![id_field]);
            query_parser.parse_query(query).expect("should parse query")
        };

        let count = query.count(&searcher).expect("should get count");

        log::debug!("Query: {query:?}");

        let collector = TopDocs::with_limit(take).and_offset(skip);

        let top_docs = searcher.search(&query, &collector).expect("should rank");

        let ids = top_docs
            .into_iter()
            .map(|(_, doc_address)| {
                use tantivy::schema::Value;

                let retrieved_doc: tantivy::TantivyDocument =
                    searcher.doc(doc_address).expect("should get doc");

                retrieved_doc
                    .get_first(id_field)
                    .expect("should retrieve id")
                    .as_str()
                    .expect("should get id")
                    .to_owned()
            })
            .collect();

        Ok(SearchResult {
            hits_count: count,
            ids,
        })
    }

    pub fn bulk_remove(&mut self, ids: &Vec<String>, wait_for: bool) -> std::io::Result<()> {
        let schema = self.data.schema();
        let id_field = schema.get_field("_id").unwrap();

        for id in ids {
            self.index_writer
                .delete_term(tantivy::Term::from_field_text(id_field, id));
        }

        self.index_writer.commit().expect("should write");

        if wait_for {
            self.index_reader.reload().expect("should reload");
        }

        Ok(())
    }

    pub fn remove_item(&mut self, id: &str, wait_for: bool) -> std::io::Result<()> {
        let schema = self.data.schema();
        let id_field = schema.get_field("_id").unwrap();

        self.index_writer
            .delete_term(tantivy::Term::from_field_text(id_field, id));

        self.index_writer.commit().expect("should write");

        if wait_for {
            self.index_reader.reload().expect("should reload");
        }

        Ok(())
    }

    pub fn index_items(&mut self, items: &[Value], wait_for: bool) -> std::io::Result<()> {
        for item in items {
            let doc = tantivy::TantivyDocument::parse_json(
                &self.data.schema(),
                &serde_json::to_string(&item)?,
            )
            .expect("should write");

            self.index_writer.add_document(doc).expect("should write");
        }

        self.index_writer.commit().expect("should write");

        if wait_for {
            self.index_reader.reload().expect("should reload");
        }

        Ok(())
    }

    pub fn new<P: AsRef<Path>>(
        name: &str,
        path: P,
        schema_def: SchemaDef,
    ) -> std::io::Result<Self> {
        use tantivy::schema::{
            IndexRecordOption, TextFieldIndexing, TextOptions, FAST, INDEXED, STRING, TEXT,
        };

        let path = path.as_ref();
        if !path.exists() {
            std::fs::create_dir_all(path)?;
        }

        log::debug!("Creating search index at {path:?}");

        let mut schema_builder = tantivy::schema::Schema::builder();

        schema_builder.add_text_field(
            "_id",
            TextOptions::default()
                .set_indexing_options(
                    TextFieldIndexing::default()
                        .set_index_option(IndexRecordOption::WithFreqs)
                        .set_tokenizer("raw"),
                )
                .set_fast(None)
                .set_stored(),
        );

        for (field_name, field) in &schema_def {
            match field {
                FieldType::Text => schema_builder.add_text_field(field_name, TEXT),
                FieldType::Keyword => schema_builder.add_text_field(field_name, STRING | FAST),
                FieldType::Integer => schema_builder.add_i64_field(field_name, INDEXED | FAST),
                FieldType::Boolean => schema_builder.add_bool_field(field_name, INDEXED),
                FieldType::Json => schema_builder.add_json_field(field_name, TEXT),
            };
        }

        let schema = schema_builder.build();

        let data = match tantivy::Index::create_in_dir(path, schema) {
            Ok(data) => data,
            Err(e) => match e {
                tantivy::TantivyError::IndexAlreadyExists => {
                    tantivy::Index::open_in_dir(path).unwrap()
                }
                e => panic!("{e:?}"),
            },
        };

        let index_writer = data.writer(50_000_000).expect("should create index writer");

        let index_reader = data
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommitWithDelay)
            .try_into()
            .expect("should do");

        Ok(Self {
            name: name.to_owned(),
            path: path.into(),
            schema_def,
            data,
            index_writer,
            index_reader,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::json;
    use tantivy::Order;

    #[test]
    fn search_index_aggregate_buckets() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("studio".to_owned(), FieldType::Keyword);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[
                json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "studio": "vixen"
                }),
                json!({
                    "_id": "1",
                    "name": "Octavia Green",
                    "studio": "tushy"
                }),
                json!({
                    "_id": "2",
                    "name": "Octavia Blue",
                    "studio": "vixen"
                }),
            ],
            true,
        )?;

        let elasticsearch_compatible_json_req = r#"
            {
                "count": {
                    "terms": {
                        "field": "studio"
                    }
                }
            }
        "#;

        let agg_req: Aggregations =
            serde_json::from_str(elasticsearch_compatible_json_req).unwrap();

        let result = index.aggregate(agg_req)?;
        let json: Value = serde_json::to_value(&result).unwrap();

        assert_eq!(
            json["count"]["buckets"][0]["key"].as_str().unwrap(),
            "vixen"
        );
        assert_eq!(
            json["count"]["buckets"][0]["doc_count"].as_u64().unwrap(),
            2
        );

        assert_eq!(
            json["count"]["buckets"][1]["key"].as_str().unwrap(),
            "tushy"
        );
        assert_eq!(
            json["count"]["buckets"][1]["doc_count"].as_u64().unwrap(),
            1
        );

        Ok(())
    }

    #[test]
    fn search_index_aggregate() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("score".to_owned(), FieldType::Integer);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[
                json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "score": 10
                }),
                json!({
                    "_id": "1",
                    "name": "Octavia Green",
                    "score": 7
                }),
                json!({
                    "_id": "2",
                    "name": "Octavia Blue",
                    "score": 8
                }),
            ],
            true,
        )?;

        let elasticsearch_compatible_json_req = r#"
        {
            "average": {
                "avg": { "field": "score" }
            }
        }
        "#;

        let agg_req: Aggregations =
            serde_json::from_str(elasticsearch_compatible_json_req).unwrap();

        let result = index.aggregate(agg_req)?;
        let json: Value = serde_json::to_value(&result).unwrap();
        assert_eq!(json["average"]["value"].as_f64().unwrap().floor(), 8.0);

        Ok(())
    }

    #[test]
    fn search_index_update() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[json!({
                "_id": "0",
                "name": "Octavia Red",
            })],
            true,
        )?;
        let result = index.search("name:red", 0, 10)?;
        assert_eq!(result.ids, &["0".to_owned()]);

        index.remove_item("0", true)?;
        index.index_items(
            &[json!({
                "_id": "0",
                "name": "Octavia Green",
            })],
            true,
        )?;

        let result = index.search("name:red", 0, 10)?;
        assert!(result.ids.is_empty());

        let result = index.search("name:green", 0, 10)?;
        assert_eq!(result.ids, &["0".to_owned()]);

        Ok(())
    }

    #[test]
    fn search_index_order_by_number() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("name.raw".to_owned(), FieldType::Keyword);
            map.insert("views".to_owned(), FieldType::Integer);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[
                json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "views": 20,
                }),
                json!({
                    "_id": "1",
                    "name": "Ashley Red",
                    "views": 10,
                }),
                json!({
                    "_id": "2",
                    "name": "Ginebra Bellucci",
                    "views": 50,
                }),
                json!({
                    "_id": "3",
                    "name": "Agatha Vega",
                    "views": 40,
                }),
            ],
            true,
        )?;

        let result = index.search_ordered("", 0, 10, ("views", Order::Desc))?;
        assert_eq!(
            result.ids,
            &[
                "2".to_owned(),
                "3".to_owned(),
                "0".to_owned(),
                "1".to_owned(),
            ]
        );

        Ok(())
    }

    #[test]
    fn search_index_basic() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("studio_names".to_owned(), FieldType::Text);
            map.insert("studio_ids".to_owned(), FieldType::Keyword);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[
                json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "studio_names": ["vixen"],
                    "studio_ids": ["vixen"]
                }),
                json!({
                    "_id": "1",
                    "name": "Ashley Red",
                    "studio_ids": []
                }),
                json!({
                    "_id": "2",
                    "name": "Ginebra Bellucci",
                    "studio_ids": []
                }),
                json!({
                    "_id": "3",
                    "name": "Agatha Vega",
                    "studio_names": ["tushy-raw"],
                    "studio_ids": ["tushy-raw"]
                }),
            ],
            true,
        )?;

        let result = index.search("name:octavia", 0, 10)?;
        assert_eq!(result.ids, &["0".to_owned()]);

        let result = index.search("name:\"octavia red\"", 0, 10)?;
        assert_eq!(result.ids, &["0".to_owned()]);

        let mut result = index.search("name:red", 0, 10)?;
        result.ids.sort();
        assert_eq!(result.ids, &["0".to_owned(), "1".to_owned()]);

        let result = index.search("name:jill", 0, 10)?;
        assert!(result.ids.is_empty());

        let result = index.search("studio_ids:\"raw\"", 0, 10)?;
        assert!(result.ids.is_empty());

        let result = index.search("studio_ids:\"tushy-raw\"", 0, 10)?;
        assert_eq!(result.ids, &["3".to_owned()]);

        let result = index.search("studio_names:\"raw\"", 0, 10)?;
        assert_eq!(result.ids, &["3".to_owned()]);

        Ok(())
    }

    #[test]
    fn search_index_or() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("studio_ids".to_owned(), FieldType::Keyword);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;

        index.index_items(
            &[
                json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "studio_ids": ["vixen"]
                }),
                json!({
                    "_id": "1",
                    "name": "Ashley Red",
                    "studio_ids": []
                }),
                json!({
                    "_id": "2",
                    "name": "Ginebra Bellucci",
                    "studio_ids": ["vixen", "tushy"]
                }),
            ],
            true,
        )?;

        let mut result = index.search("studio_ids:vixen OR studio_ids:tushy", 0, 10)?;
        result.ids.sort();
        assert_eq!(result.ids, &["0".to_owned(), "2".to_owned()]);

        let mut result = index.search(
            "(studio_ids:vixen OR studio_ids:tushy) AND name:ginebra",
            0,
            10,
        )?;
        result.ids.sort();
        assert_eq!(result.ids, &["2".to_owned()]);

        Ok(())
    }

    #[test]
    fn search_index_recovery() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("studio_names".to_owned(), FieldType::Text);
            map.insert("studio_ids".to_owned(), FieldType::Keyword);
            map
        };

        {
            let mut index = SearchIndex::new("default", &path, schema.clone())?;
            assert_eq!(0, index.count(None)?);

            index.index_items(
                &[json!({
                    "_id": "0",
                    "name": "Octavia Red",
                    "studio_names": ["vixen"],
                    "studio_ids": ["vixen"]
                })],
                true,
            )?;
            assert_eq!(1, index.count(None)?);

            index.index_items(
                &[json!({
                    "_id": "1",
                    "name": "Agatha Vega",
                    "studio_names": ["vixen"],
                    "studio_ids": ["vixen"]
                })],
                true,
            )?;
            assert_eq!(2, index.count(None)?);
        }

        {
            let index = SearchIndex::new("default", &path, schema)?;
            assert_eq!(2, index.count(None)?);
        }

        Ok(())
    }

    #[test]
    fn search_index_deletion() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map.insert("studio_names".to_owned(), FieldType::Text);
            map.insert("studio_ids".to_owned(), FieldType::Keyword);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;
        assert_eq!(0, index.count(None)?);

        index.index_items(
            &[json!({
                "_id": "0",
                "name": "Octavia Red",
                "studio_names": ["vixen"],
                "studio_ids": ["vixen"]
            })],
            true,
        )?;
        assert_eq!(1, index.count(None)?);

        index.index_items(
            &[json!({
                "_id": "1",
                "name": "Agatha Vega",
                "studio_names": ["vixen"],
                "studio_ids": ["vixen"]
            })],
            true,
        )?;
        assert_eq!(2, index.count(None)?);

        index.remove_item("0", true)?;
        assert_eq!(1, index.count(None)?);

        index.remove_item("1", true)?;
        assert_eq!(0, index.count(None)?);

        Ok(())
    }

    #[test]
    fn search_index_bulk_deletion() -> std::io::Result<()> {
        let path = tempfile::tempdir()?;

        let schema = {
            let mut map = HashMap::new();
            map.insert("name".to_owned(), FieldType::Text);
            map
        };

        let mut index = SearchIndex::new("default", &path, schema)?;
        assert_eq!(0, index.count(None)?);

        index.index_items(
            &[json!({
                "_id": "0",
                "name": "Octavia Red",
            })],
            true,
        )?;
        assert_eq!(1, index.count(None)?);

        index.index_items(
            &[json!({
                "_id": "1",
                "name": "Agatha Vega",
            })],
            true,
        )?;
        assert_eq!(2, index.count(None)?);

        index.bulk_remove(&vec!["0".to_owned(), "1".to_owned()], true)?;
        assert_eq!(0, index.count(None)?);

        Ok(())
    }
}
