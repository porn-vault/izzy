#[macro_use]
extern crate rocket;

mod collection;
mod response;
#[cfg(feature = "search")]
mod search_index;
mod util;

use rocket::data::{Limits, ToByteUnit};
use rocket::serde::json::{json, Json};
use serde_json::Value;
use simple_logger::SimpleLogger;
use std::num::ParseIntError;
use std::sync::Arc;

#[get("/")]
fn root() -> Json<Value> {
    Json(json!({
      "name": env!("CARGO_CRATE_NAME"),
      "version": env!("CARGO_PKG_VERSION"),
    }))
}

#[post("/exit")]
fn exit() -> Json<Value> {
    std::process::exit(0);

    #[allow(unreachable_code)]
    Json(json!({}))
}

fn get_port_from_args() -> Result<Option<u16>, ParseIntError> {
    let args: Vec<String> = std::env::args().collect();

    for (i, arg) in args.iter().enumerate() {
        if arg == "--port" {
            let port_num = args[i + 1].parse()?;
            return Ok(Some(port_num));
        }
    }

    Ok(None)
}

#[launch]
fn rocket() -> _ {
    SimpleLogger::new().env().init().unwrap();

    log::info!(target: "main", "Starting up izzy");

    let port = get_port_from_args().unwrap().unwrap_or(7999);

    let figment = rocket::Config::figment()
        .merge(("port", port))
        .merge(("limits", Limits::new().limit("json", 50.mebibytes())));

    rocket::custom(figment)
        .manage(Arc::new(collection::routes::MyState::default()))
        .manage(Arc::new(search_index::routes::MyState::default()))
        .mount("/", routes![exit, root])
        .mount("/collection", collection::routes::routes())
        .mount("/search", search_index::routes::routes())
}
